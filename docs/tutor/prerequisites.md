# Prerequisites

1. A positive mindset
2. A mobile device or an electronic device with a stable internet connection if teaching online
3. Response to emails

## Completing Profile

Upon completion, Tutor is given a public profile and sensitive information is hidden from the public. An user will be required to login and pay a flat rate fee to view your contact details. Profile consists of:

1. Basic details
    - Name (First and Last)
    - Verified Mobile number
    - Email address
    - Gender
    - Profile Picture (optional)
    - Date of Birth
    - Approximate Address (We do not accept your street address or house number due to security reasons. You would only need the closest city)
2. Subjects you teach
3. Education details
4. Teaching experience
5. Fees structure and Fee details
6. Work Experience (optional)
7. Availability to Travel to Student and related details
8. Availability for Assignment helping
9. Profile Description

Tutors profile will not be public if any of the required details not listed.
