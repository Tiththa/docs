# Getting Started as a Tutor

Tutors who wish to join Acahelp must be reside in our [supported country list](/guide/supported-countries).

Acahelp offers Tutors from the countries supported, an easy all-aged platform to find their jobs easy. Tutors when they are in retirement ages, find it hard to stay without teaching. The technology platforms are too complex for non-technical tutors to understand. Acahelp offers a simple job platform without hassle.

Acahelp offers a special service for retired talented tutors, non-technical tutors who find it difficult to find a way to continue their passion and earn an extra income. If you are a non-technical tutor residing in one of our supported countries list, please drop us an email to support@acahelp.net. An agent will look into your matter and help you to join the platform.

While Acahelp offers, you may need to have sound knowledge on
- Conducting online classes if tutor is willing to teach online
- Traveling method if tutor is traveling to user's place
- Handling emails and responding
- Own a mobile number for verifying purposes

## Benefits for Tutors :heavy_check_mark:

- Find jobs for free, exclusive for the newly joining Tutors
- Find Online, Home Tutoring and Assignment helping jobs without a hassle
- A modern public profile you could share across your contacts
- No overwhelming user interfaces
- No Online chat: Acahelp did not implement an internal chat between Tutors and Members as both parties are given contact details to contact personally.
- Acahelp does not allow third-party services to handle your personal details
- Acahelp does not make your details public, Details only visible for logged in members who have paid Acahelp to view Tutor contact details beside the public profile
- Able to accept global payments via Payoneer. Acahelp does not interfere with your payments or manage your payments.
