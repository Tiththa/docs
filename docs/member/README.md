# Getting Started as a Member

Members who wish to use Acahelp must be reside in our [supported country list](/guide/supported-countries).

Acahelp offers a simple method to list requirements for both individuals and customers representing companies and institutions. One simple account for both, simplified for you.

You can list your requirements as two types:

- **As an individual** - If you are a Student/Parent of a Student or a customer who is not entitled to a company or an Institution, you could post your requirements as an Individual
- **As a company/Institution** -  If you are an employee/a representative of a company or an Institution, you are required to post as a company/Institution.

## Benefits for Members :heavy_check_mark:

- Post job requirements easy and fast
- Find Tutors from the [supported country list](/guide/supported-countries)
- Find Online, Home Tutoring and Assignment helping jobs without a hassle
- No overwhelming user interfaces
- No Online chat: Acahelp did not implement an internal chat between Tutors and Members as both parties are given contact details to contact personally.
- Acahelp does not allow third-party services to handle your personal details
- Acahelp does not make your details public, details only visible for logged in tutors who have paid Acahelp to view Job listee's contact details.
