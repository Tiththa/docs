# Posting requirement as an Individual
## Prerequisites

- A mobile device to verify your account
- Access to an email address

Acahelp offers to sign up via
- Native email address
- Social Login
  - Google
  - LinkedIn

Post your requirement by simply navigating to "Post Job" in the sidebar. List your requirements according to your needs.

Acahelp respects about your security and we have limited the location scope to City only, means that it is not advisable to list your street name or house no.

::: warning Note
  Acahelp is only a listing platform and after you find a suitable Tutor you may disclose your location **only** if needed via the provided contact details. Therefore we have increased our security measures to limit the address scope to just city and the country.
:::

## Who are identified as individuals ?

- You must not be representing a Company or an Institution in the intention to find Tutors for your Company or Institution.
- You can be either a Student, Parent or any other customer except the parties aforementioned.

## Dos and Don'ts

- Select your closest city or hometown for the location
- Select the Request type (Whether you want to find a Tutor or a Tutor to help with your Assignment)
- Summarize your requirement in the following format and **refrain** from sharing any contact details (Mobile numbers, email addresses, websites or links). You are able to embrace font formatting and listing details in the bulletin order.

::: tip Format
Example: I am looking for a qualified English teacher for my son. He is a grade 6 boy with basic knowledge in English. Looking for somebody who could teach him English grammar, spoken English, Online or visiting classes as home. My max budget is $20 per day.
:::

- Enter your subjects and the level
- Specify meeting methods and the Budget details
- Choose the post type as **"Individual"**
- Submit your job requirement and should be online

Simple as that!

## Listing Fee and Duration

Acahelp does not charge individuals a listing fee. Yes, it is free of charge!.

A Job listing would last for 30 Days.

## What to do when a Tutor is found?

It is advisable that you login to your account and delete the job requirement as Tutors may contact you for your listing. We will notify you after each tutor contacts you to delete your job requirement if a suitable tutor is found.

If you fail to close your requirement and Tutors have reported your requirement several times, Acahelp may delete your job listing automatically. This is to ensure to maintain the quality of the platform.

## How to pay a Tutor ?

Acahelp does not handle payments internally yet. Therefore all payments are made by you directly to the Tutors. It is advisable that you contact your Tutor for a working payment method.

::: tip
Acahelp recommends that you make an advance payment to the Tutor to gain trust. If you have any complaints, please do not hesitate to contact [support@acahelp.net](mailto::support@acahelp.net)
:::

## Limitations

While there are no limitations for individuals to post their requirements, Individuals could post many job requirement listings as they wish. However Acahelp monitors job requirement listings for excessive listing job requirements per account.

One user may have one account only and the mobile number can be used only once in our platform. If you have any issues regarding verifying your mobile or your account, Acahelp would be more than grateful to assist you via [support@acahelp.net](mailto::support@acahelp.net)

### Profile and Account Deletion

Acahelp provides you a simple and smooth interface to edit your details and deleting your account as well.

::: danger
Deleting your account will not make you eligible to re-register to Acahelp platform. All the resources and data in the account will be deleted.
:::

### Violating the Terms and Conditions

Violating the terms and conditions, providing malicious code or involving in money laundering activities will immediately ban you from Acahelp platform.
