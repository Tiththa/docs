# Posting requirement as a Company

## Prerequisites

- A mobile device to verify your account
- Access to an email address

Acahelp offers to sign up via
- Native email address
- Social Login
  - Google
  - LinkedIn

## Benefits of posting as a Company/Institution

- You are able to feature your listing in the homepage
- Highlight your listing in the ["all jobs page"](https://acahelp.net/jobs) in light gold colour
- Pin your listing in the ["all jobs page"](https://acahelp.net/jobs), so your listing always be the first to appear for two weeks
- Showcase your logo of your company and the name of the company in your listing

All the listed benefits are available for your as addons at a reasonable price

## Pricing

- An ordinary listing is priced at $12
- **Addon:** Show your company logo in the listing for $25
- **Addon:** Highlight your listing $25
- **Addon:** Pin your listing in the ["all jobs page"](https://acahelp.net/jobs) for two weeks for $40

## Coupons

Acahelp allows to obtain a discount from coupons available at the time.

## Dos and Don'ts

- Select your approximate location
- Select the Request type (Whether you want to find a Tutor or a Tutor to help with your Assignment)
- Summarize your requirement in the following format and **refrain** from sharing any contact details (Mobile numbers, email addresses, websites or links). You are able to embrace font formatting and listing details in the bulletin order.

::: tip Format
Example: We are looking for a qualified English teacher for our Institution name. Tutor must be a trained Tutor....
:::


- Enter your subjects and the level
- Specify meeting methods and the Budget details
- Choose the post type as **"Company/Institution"**
- Enter your Company name and your Company logo ***(Please not that your logo will only be visible to the public if you choose the addon to show your Company logo in the listing.)***
- Submit your job requirement and you will be redirected to the checkout page
- Choose your **addons** or continue without addons
- Enter coupon code if you have any
- Checkout and you will be billed, your listing will be public instantly after a successful payment attempt

Simple as that!

## Billing

Each transaction will be billed and invoices would be sent out to your registered email address by our payment provider. Any issue with the payments (Card issues or bank issues), must be directed to [Paddle Support Center](https://paddle.net).

## What to do when a Tutor is found?

It is advisable that you login to your account and delete the job requirement as Tutors may contact you for your listing. We will notify you after each tutor contacts you to delete your job requirement if a suitable tutor is found.

If you fail to close your requirement and Tutors have reported your requirement several times, Acahelp may delete your job listing automatically. This is to ensure to maintain the quality of the platform.

## How to pay a Tutor ?

Acahelp does not handle payments internally yet. Therefore all payments are made by you directly to the Tutors. It is advisable that you contact your Tutor for a working payment method.

::: tip
Acahelp recommends that you make an advance payment to the Tutor to gain trust. If you have any complaints, please do not hesitate to contact [support@acahelp.net](mailto::support@acahelp.net)
:::

## Limitations

One user may have one account only and the mobile number can be used only once in our platform. If you have any issues regarding verifying your mobile or your account, Acahelp would be more than grateful to assist you via [support@acahelp.net](mailto::support@acahelp.net)

### Profile and Account Deletion

Acahelp provides you a simple and smooth interface to edit your details and deleting your account as well.

::: danger
Deleting your account will not make you eligible to re-register to Acahelp platform. All the resources and data in the account will be deleted.
:::

### Violating the Terms and Conditions

Violating the terms and conditions, providing malicious code or involving in money laundering activities will immediately ban you from Acahelp platform.
