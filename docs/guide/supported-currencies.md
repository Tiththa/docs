# Supported Currencies

As a global platform, Acahelp currently offers users the ability to post their job requirements and apply for jobs in many different currencies.

Below are answers for more frequently asked questions we receive regarding currencies, and how currencies are assigned to Acahelp user accounts.

- [Which currencies does Acahelp support?](#which-currencies-does-acahelp-support)
- [How is my account’s currency determined?](#how-is-my-accounts-currency-determined)
- [How can I change the currency for my account?](#how-can-i-change-the-currency-for-my-account)
- [How are you calculating exchange rates?](#how-are-you-calculating-exchange-rates)


## Which currencies does Acahelp support?

Currently Acahelp offers users the ability to post their job requirements and apply for jobs in the following currencies:

<table>

   <tbody>
      <tr>
            <td>AED - United Arab Emirates Dirham</td>
            <td>AFN - Afghan Afghani</td>
         </tr>
      <tr>
            <td>ALL - Albanian Lek</td>
            <td>AMD - Armenian Dram</td>
         </tr>
      <tr>
            <td>ANG - Netherlands Antillean Guilder</td>
            <td>AOA - Angolan Kwanza</td>
         </tr>
      <tr>
            <td>ARS - Argentine Peso</td>
            <td>AUD - Australian Dollar</td>
         </tr>
      <tr>
            <td>AWG - Aruban Florin</td>
            <td>AZN - Azerbaijani Manat</td>
         </tr>
      <tr>
            <td>BAM - Bosnia-Herzegovina Convertible Mark</td>
            <td>BBD - Barbadian Dollar</td>
         </tr>
      <tr>
            <td>BDT - Bangladeshi Taka</td>
            <td>BGN - Bulgarian Lev</td>
         </tr>
      <tr>
            <td>BHD - Bahraini Dinar</td>
            <td>BIF - Burundian Franc</td>
         </tr>
      <tr>
            <td>BMD - Bermudan Dollar</td>
            <td>BND - Brunei Dollar</td>
         </tr>
      <tr>
            <td>BOB - Bolivian Boliviano</td>
            <td>BRL - Brazilian Real</td>
         </tr>
      <tr>
            <td>BSD - Bahamian Dollar</td>
            <td>BTC - Bitcoin</td>
         </tr>
      <tr>
            <td>BTN - Bhutanese Ngultrum</td>
            <td>BWP - Botswanan Pula</td>
         </tr>
      <tr>
            <td>BYR - Belarusian Ruble</td>
            <td>BYN - New Belarusian Ruble</td>
         </tr>
      <tr>
            <td>BZD - Belize Dollar</td>
            <td>CAD - Canadian Dollar</td>
         </tr>
      <tr>
            <td>CDF - Congolese Franc</td>
            <td>CHF - Swiss Franc</td>
         </tr>
      <tr>
            <td>CLF - Chilean Unit of Account (UF)</td>
            <td>CLP - Chilean Peso</td>
         </tr>
      <tr>
            <td>CNY - Chinese Yuan</td>
            <td>COP - Colombian Peso</td>
         </tr>
      <tr>
            <td>CRC - Costa Rican Colón</td>
            <td>CUC - Cuban Convertible Peso</td>
         </tr>
      <tr>
            <td>CUP - Cuban Peso</td>
            <td>CVE - Cape Verdean Escudo</td>
         </tr>
      <tr>
            <td>CZK - Czech Republic Koruna</td>
            <td>DJF - Djiboutian Franc</td>
         </tr>
      <tr>
            <td>DKK - Danish Krone</td>
            <td>DOP - Dominican Peso</td>
         </tr>
      <tr>
            <td>DZD - Algerian Dinar</td>
            <td>EGP - Egyptian Pound</td>
         </tr>
      <tr>
            <td>ERN - Eritrean Nakfa</td>
            <td>ETB - Ethiopian Birr</td>
         </tr>
      <tr>
            <td>EUR - Euro</td>
            <td>FJD - Fijian Dollar</td>
         </tr>
      <tr>
            <td>FKP - Falkland Islands Pound</td>
            <td>GBP - British Pound Sterling</td>
         </tr>
      <tr>
            <td>GEL - Georgian Lari</td>
            <td>GGP - Guernsey Pound</td>
         </tr>
      <tr>
            <td>GHS - Ghanaian Cedi</td>
            <td>GIP - Gibraltar Pound</td>
         </tr>
      <tr>
            <td>GMD - Gambian Dalasi</td>
            <td>GNF - Guinean Franc</td>
         </tr>
      <tr>
            <td>GTQ - Guatemalan Quetzal</td>
            <td>GYD - Guyanaese Dollar</td>
         </tr>
      <tr>
            <td>HKD - Hong Kong Dollar</td>
            <td>HNL - Honduran Lempira</td>
         </tr>
      <tr>
            <td>HRK - Croatian Kuna</td>
            <td>HTG - Haitian Gourde</td>
         </tr>
      <tr>
            <td>HUF - Hungarian Forint</td>
            <td>IDR - Indonesian Rupiah</td>
         </tr>
      <tr>
            <td>ILS - Israeli New Sheqel</td>
            <td>IMP - Manx pound</td>
         </tr>
      <tr>
            <td>INR - Indian Rupee</td>
            <td>IQD - Iraqi Dinar</td>
         </tr>
      <tr>
            <td>IRR - Iranian Rial</td>
            <td>ISK - Icelandic Króna</td>
         </tr>
      <tr>
            <td>JEP - Jersey Pound</td>
            <td>JMD - Jamaican Dollar</td>
         </tr>
      <tr>
            <td>JOD - Jordanian Dinar</td>
            <td>JPY - Japanese Yen</td>
         </tr>
      <tr>
            <td>KES - Kenyan Shilling</td>
            <td>KGS - Kyrgystani Som</td>
         </tr>
      <tr>
            <td>KHR - Cambodian Riel</td>
            <td>KMF - Comorian Franc</td>
         </tr>
      <tr>
            <td>KPW - North Korean Won</td>
            <td>KRW - South Korean Won</td>
         </tr>
      <tr>
            <td>KWD - Kuwaiti Dinar</td>
            <td>KYD - Cayman Islands Dollar</td>
         </tr>
      <tr>
            <td>KZT - Kazakhstani Tenge</td>
            <td>LAK - Laotian Kip</td>
         </tr>
      <tr>
            <td>LBP - Lebanese Pound</td>
            <td>LKR - Sri Lankan Rupee</td>
         </tr>
      <tr>
            <td>LRD - Liberian Dollar</td>
            <td>LSL - Lesotho Loti</td>
         </tr>
      <tr>
            <td>LTL - Lithuanian Litas</td>
            <td>LVL - Latvian Lats</td>
         </tr>
      <tr>
            <td>LYD - Libyan Dinar</td>
            <td>MAD - Moroccan Dirham</td>
         </tr>
      <tr>
            <td>MDL - Moldovan Leu</td>
            <td>MGA - Malagasy Ariary</td>
         </tr>
      <tr>
            <td>MKD - Macedonian Denar</td>
            <td>MMK - Myanma Kyat</td>
         </tr>
      <tr>
            <td>MNT - Mongolian Tugrik</td>
            <td>MOP - Macanese Pataca</td>
         </tr>
      <tr>
            <td>MRO - Mauritanian Ouguiya</td>
            <td>MUR - Mauritian Rupee</td>
         </tr>
      <tr>
            <td>MVR - Maldivian Rufiyaa</td>
            <td>MWK - Malawian Kwacha</td>
         </tr>
      <tr>
            <td>MXN - Mexican Peso</td>
            <td>MYR - Malaysian Ringgit</td>
         </tr>
      <tr>
            <td>MZN - Mozambican Metical</td>
            <td>NAD - Namibian Dollar</td>
         </tr>
      <tr>
            <td>NGN - Nigerian Naira</td>
            <td>NIO - Nicaraguan Córdoba</td>
         </tr>
      <tr>
            <td>NOK - Norwegian Krone</td>
            <td>NPR - Nepalese Rupee</td>
         </tr>
      <tr>
            <td>NZD - New Zealand Dollar</td>
            <td>OMR - Omani Rial</td>
         </tr>
      <tr>
            <td>PAB - Panamanian Balboa</td>
            <td>PEN - Peruvian Nuevo Sol</td>
         </tr>
      <tr>
            <td>PGK - Papua New Guinean Kina</td>
            <td>PHP - Philippine Peso</td>
         </tr>
      <tr>
            <td>PKR - Pakistani Rupee</td>
            <td>PLN - Polish Zloty</td>
         </tr>
      <tr>
            <td>PYG - Paraguayan Guarani</td>
            <td>QAR - Qatari Rial</td>
         </tr>
      <tr>
            <td>RON - Romanian Leu</td>
            <td>RSD - Serbian Dinar</td>
         </tr>
      <tr>
            <td>RUB - Russian Ruble</td>
            <td>RWF - Rwandan Franc</td>
         </tr>
      <tr>
            <td>SAR - Saudi Riyal</td>
            <td>SBD - Solomon Islands Dollar</td>
         </tr>
      <tr>
            <td>SCR - Seychellois Rupee</td>
            <td>SDG - Sudanese Pound</td>
         </tr>
      <tr>
            <td>SEK - Swedish Krona</td>
            <td>SGD - Singapore Dollar</td>
         </tr>
      <tr>
            <td>SHP - Saint Helena Pound</td>
            <td>SLL - Sierra Leonean Leone</td>
         </tr>
      <tr>
            <td>SOS - Somali Shilling</td>
            <td>SRD - Surinamese Dollar</td>
         </tr>
      <tr>
            <td>STD - São Tomé and Príncipe Dobra</td>
            <td>SVC - Salvadoran Colón</td>
         </tr>
      <tr>
            <td>SYP - Syrian Pound</td>
            <td>SZL - Swazi Lilangeni</td>
         </tr>
      <tr>
            <td>THB - Thai Baht</td>
            <td>TJS - Tajikistani Somoni</td>
         </tr>
      <tr>
            <td>TMT - Turkmenistani Manat</td>
            <td>TND - Tunisian Dinar</td>
         </tr>
      <tr>
            <td>TOP - Tongan Paʻanga</td>
            <td>TRY - Turkish Lira</td>
         </tr>
      <tr>
            <td>TTD - Trinidad and Tobago Dollar</td>
            <td>TWD - New Taiwan Dollar</td>
         </tr>
      <tr>
            <td>TZS - Tanzanian Shilling</td>
            <td>UAH - Ukrainian Hryvnia</td>
         </tr>
      <tr>
            <td>UGX - Ugandan Shilling</td>
            <td>USD - United States Dollar</td>
         </tr>
      <tr>
            <td>UYU - Uruguayan Peso</td>
            <td>UZS - Uzbekistan Som</td>
         </tr>
      <tr>
            <td>VEF - Venezuelan Bolívar Fuerte</td>
            <td>VND - Vietnamese Dong</td>
         </tr>
      <tr>
            <td>VUV - Vanuatu Vatu</td>
            <td>WST - Samoan Tala</td>
         </tr>
      <tr>
            <td>XAF - CFA Franc BEAC</td>
            <td>XAG - Silver (troy ounce)</td>
         </tr>
      <tr>
            <td>XAU - Gold (troy ounce)</td>
            <td>XCD - East Caribbean Dollar</td>
         </tr>
      <tr>
            <td>XDR - Special Drawing Rights</td>
            <td>XOF - CFA Franc BCEAO</td>
         </tr>
      <tr>
            <td>XPF - CFP Franc</td>
            <td>YER - Yemeni Rial</td>
         </tr>
      <tr>
            <td>ZAR - South African Rand</td>
            <td>ZMK - Zambian Kwacha (pre-2013)</td>
         </tr>
      <tr>
            <td>ZMW - Zambian Kwacha</td>
            <td>ZWL - Zimbabwean Dollar</td>
         </tr>
      </tbody>
</table>

By default, users or tutors who do not have one of our supported currencies assigned to their Acahelp account, will see prices set in USD.

## How is my account’s currency determined?

The currency is set based on the IP location of where your Acahelp account was created. By default, users or tutors who do not have one of our supported currencies assigned to their Acahelp account, will see prices set in USD.

## How can I change the currency for my account?

We do not permit users to change the currency of the account. If you registered as a Tutor and reside in a different nation than the one your Acahelp account was created in, and are presently located in the country of your residence, please contact our [support team](mailto:support@acahelp.net) so we can assist you further.

## How are you calculating exchange rates?

Acahelp uses a country-specific price tier matrix to determine a user's localized price. You can see the price tier matrix [here](#).  The matrix will be updated weekly based on market conditions.
