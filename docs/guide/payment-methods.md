# Payment Methods on Acahelp

Acahelp supports one global payment method, depending on your account country and location. In accordance with the payment method, Acahelp is unable to support users operating from sanctioned countries and restricted countries. You can find the list below. Transactions originating from comprehensively sanctioned countries or geographical regions will be rejected by our Payment provider.

::: tip Note
This is not relevant for Tutors. We do not handle Tutor payments internally. Payment method is for users when checking out to view Tutor contact details and for Tutors to checking our to apply for a job listing.
:::

## Sanctioned Countries

- Crimea (Region of Ukraine)
- Cuba
- Iran
- Libya
- North Korea
- Nicaragua
- Somalia
- Sudan
- Syria
- Venezuela
- Yemen

## Restricted countries

- Belarus
- Burma (Myanmar)
- Cote d’Ivoire
- Democratic Republic of the Congo
- Iraq
- Liberia (Former Regime of Charles Taylor)
- Sierra Leone
- Zimbabwe

These lists are subject to change from our payment provider.
