module.exports = {
  lang: 'en-US',
  title: 'Acahelp Docs',
  description: 'This is my first VuePress site',

  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  themeConfig: {
    logo: '/assets/favicon.png',
    editLinks: false,
    darkMode: false,
    contributors: false,
    lastUpdated: false,
    navbar: [
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'Member Topics',
        link: '/member/',
      },
      {
        text: 'Tutor Topics',
        link: '/tutor/',
      },
      {
        text: 'Go to Acahelp',
        link: 'https://acahelp.net'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            'supported-countries',
            'supported-currencies',
            'payment-methods',
          ]
        }
      ],
      '/member/': [
        {
          title: 'Getting Started as a Member',
          collapsable: false,
          children: [
            'individual-posting',
            'company-posting'
          ]
        }
      ],
      '/tutor/': [
        {
          title: 'Getting Started as a Tutor',
          collapsable: false,
          children: [
            'prerequisites',
            'fees-structure',
          ]
        }
      ],
    }
  },

  plugins: [
    [
      '@vuepress/plugin-search'
    ],
  ],
}
